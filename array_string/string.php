<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>String PHP</title>
    </head>
    <body>
        <h2>Berlatih String PHP</h2>
        <h4>SUPRI ANDRIANO</h4>
        <p>
            <?php   
            echo "<h3>Soal No 1</h3>";
            $kalimat    ="PHP is never old";
            ?>
            kalimat: <u><?php echo $kalimat?></u>
            <p>
            <p>
                <?php
                $jumlah_karakter    =strlen($kalimat);
                echo "Jumlah Karakter=$jumlah_karakter karakter";
            ?>
            </p>

            <?php
                $string = "PHP is never old";
            ?>
            Kalimat: <u><?php echo $string?></u>
            </p>
            <p>
                <?php
                $Jumlah_kata    =str_word_count($string);
                echo "Jumlah kata=$Jumlah_kata kata";
            ?>
        </p>

        <?php   
            $kalimat    ="Hello PHP!";
            ?>
            kalimat: <u><?php echo $kalimat?></u>
            <p>
            <p>
                <?php
                $jumlah_karakter    =strlen($kalimat);
                echo "Jumlah Karakter=$jumlah_karakter karakter";
            ?>
            </p>

            <?php
                $string = "Hello PHP!";
            ?>
            Kalimat: <u><?php echo $string?></u>
            </p>
            <p>
                <?php
                $Jumlah_kata    =str_word_count($string);
                echo "Jumlah kata=$Jumlah_kata kata";
            ?>
        </p>

        <?php   
            $kalimat    ="I'm ready for the challenges";
            ?>
            kalimat: <u><?php echo $kalimat?></u>
            <p>
            <p>
                <?php
                $jumlah_karakter    =strlen($kalimat);
                echo "Jumlah Karakter=$jumlah_karakter karakter";
            ?>
            </p>

            <?php
                $string = "I'm ready for the challenges";
            ?>
            Kalimat: <u><?php echo $string?></u>
            </p>
            <p>
                <?php
                $Jumlah_kata    =str_word_count($string);
                echo "Jumlah kata=$Jumlah_kata kata";
            ?>
        </p>

        <?php
        echo "<h3>Soal No 2</h3>";
        $kalimat = "I Love PHP";
        $sub_kalimat1 = substr($kalimat, 0, 1);
        echo $sub_kalimat1;
        ?>

        
        <?php
        echo "<h3>Soal No 2</h3>";
        $kalimat = "I Love PHP";
        $sub_kalimat2 = substr($kalimat, 1, 5);
        echo $sub_kalimat2;
        ?>


        <?php
        echo "<h3>Soal No 2</h3>";
        $kalimat = "I Love PHP";
        $sub_kalimat3 = substr($kalimat, 6, 6);
        echo $sub_kalimat3;
        ?>


        <?php
        echo "<h3>Soal No 3</h3>";
        $kalimat = "PHP is old but sexy!";
        echo strrev($kalimat);
        ?>
    </body>        
</html>