<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>String PHP</title>
    </head>
    <body>
        <h2>Berlatih Array</h2>
        <h4>SUPRI ANDRIANO</h4>
        <p>
            <?php
            echo "<h3>Soal No 1</h3>";
            $nama = array("Mike","Dustin","Will","Lucas","Max","Eleven");
            sort($nama);
            $kelompok = array();
            foreach ($nama as $index => $nama_siswa) {
                $kelompok_ke = ($index % 1) + 1;
                $kelompok[$kelompok_ke][] = $nama_siswa;
            }


            foreach ($kelompok as $nomor_kelompok => $siswa_dalam_kelompok) {
                echo "kelompok $nomor_kelompok: " . implode(", ",  $siswa_dalam_kelompok) . "<br>";
            }
            ?>

            <?php
            echo "<h3>Soal No 1</h3>";
            $nama = array("Hopper","Nancy","Joyce","Jonathan","Murray");
            sort($nama);
            $kelompok = array();
            foreach ($nama as $index => $nama_siswa) {
                $kelompok_ke = ($index % 1) + 2;
                $kelompok[$kelompok_ke][] = $nama_siswa;
            }


            foreach ($kelompok as $nomor_kelompok => $siswa_dalam_kelompok) {
                echo "kelompok $nomor_kelompok: " . implode(", ",  $siswa_dalam_kelompok) . "<br>";
            }
            ?>


            <?php
            echo "<h3>Soal No 2</h3>";
            $nama_siswa = array("Mike","Dustin","Will","Lucas","Max","Eleven");
            echo "Jumlah siswa: " . count($nama_siswa) . "<br>";
            echo "Isi array siswa: ";
            print_r($nama_siswa);
            echo "<br><br>";
            ?>


            <?php
            echo "<h3>Soal No 2</h3>";
            $nama_siswa = array("Hopper","Nancy","Joyce","Jonathan","Murray");
            echo "Jumlah siswa: " . count($nama_siswa) . "<br>";
            echo "Isi array siswa: ";
            print_r($nama_siswa);
            echo "<br><br>";
            ?>


            <?php
            echo "<h3>Soal No 3</h3>";
            $person1 = array(
                "name" => "Will Byers",
                "age" => 12,
                "aliases" => array("Will the Wise"),
                "status" => "alive"
            );

            $person2 = array(
                "name" => "Mike Wheeler",
                "age" => 12,
                "aliases" => array("Dugeon Master"),
                "status" => "alive"
            );

            $person3 = array(
                "name" => "Jim Hooper",
                "age" => 12,
                "aliases" => array("Chief Hopper"),
                "status" => "alive"
            );

            $person4 = array(
                "name" => "Eleven",
                "age" => 12,
                "aliases" => array("El"),
                "status" => "alive"
            );

            $people = array(
                $person1,
                $person2,
                $person3,
                $person4
            );

            foreach ($people as $person) {
                echo "Name: " . $person["name"] . "<br>";
                echo "Age: " . $person["age"] . "<br>";
                echo "Aliases: " . implode(", ", $person["aliases"]) . "<br>";
                echo "Status: " . $person["status"] . "<br><br>";
            }
            ?>
        </p>
    <body>
</html>
