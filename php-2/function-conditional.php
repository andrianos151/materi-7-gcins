<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible"content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>

    <?php
    echo "<h3>Soal No 1 Greetings</h3>";
    function greetings($name) {
        return "Halo, " . $name . "! Selamat datang di Sanbercode";
    }

    echo greetings("Abdul");
    ?>



    <?php
    echo "<h3>Soal No 2 Reverse String</h3>";
    function reverseString($str) {
        $len = strlen($str);
        $revStr = "";

        // For loop
        for ($i = $len - 1; $i >= 0; $i--) {
            $revStr .= $str[$i];
        }
        echo "Reverse string using for loop: " . $revStr . "\n";

        $revStr = "";
        $i = $len - 1;

        // While loop
        while ($i >= 0) {
            $revStr .= $str[$i];
            $i--;
        }
        echo "Reverse string using while loop: " . $revStr . "\n";

        $revStr = "";
        $i = $len - 1;

        // Do-while loop
        do {
            $revStr .= $str[$i];
            $i--;
        } while ($i >= 0);
        echo "Reverse string using do-while loop: " . $revStr . "\n";
    }

    reverseString("We Are Sanbers Developers");
    ?>

    <?php
    echo "<h3>Soal No 3 Palindrome</h3>";
    function isPalindrome($str) {
        $len = strlen($str);
        $isPalindrome = true;

        for ($i = 0; $i < $len / 2; $i++) {
            if ($str[$i] != $str[$len - $i - 1]) {
                $isPalindrome = false;
                break;
            }
        }

        if ($isPalindrome) {
            echo "$str is a palindrome.\n";
        } else {
            echo "$str is not a palindrome.\n";
        }
    }

    isPalindrome("katak");
    isPalindrome("hello");
    ?>

    <?php
    echo "<h3>Soal No 4 Tentukan Nilai</h3>";
    function tentukan_nilai($nilai) {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            return "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            return "Cukup";
        } else {
            return "Kurang";
        }
    }

    echo "Nilai 90 masuk kategori: " . tentukan_nilai(90) . "<br>"; // output: Nilai 90 masuk kategori: Sangat Baik
    echo "Nilai 75 masuk kategori: " . tentukan_nilai(75) . "<br>"; // output: Nilai 75 masuk kategori: Baik
    echo "Nilai 65 masuk kategori: " . tentukan_nilai(65) . "<br>"; // output: Nilai 65 masuk kategori: Cukup
    echo "Nilai 50 masuk kategori: " . tentukan_nilai(50) . "<br>"; // output: Nilai 50 masuk kategori: Kurang
    ?>

</body>    