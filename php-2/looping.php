<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Looping</title>
    <head>
    <body>
        <h1>Berlatih Looping</h1>

        <?php
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        for($i=1; $i<=20; $i++) {
            echo "i Love PHP - ".$i."<br>"; //ascending
        }
        ?>
        

        <?php
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        for($i=20; $i>=1; $i--) {
            echo "i Love PHP - ".$i,"<br>"; //descending
        }
        ?>


        <?php
        echo "<h3>Soal No 2 Looping Array Modulo</h3>";
        $angka = array(18, 45, 29, 61, 47, 34);
        $rest = array();
        foreach ($angka as $a) {
            $sisa = $a % 5;
            $rest[] = $sisa;
        }

        print_r($rest);
        ?>


        <?php
        echo "<h3>Soal No 3 Loop Assosiative Array</h3>";
        // Data dalam bentuk array multidimensi
        $items = array(
        array(1, 'Keyboard Logitech', 60000, 'Keyboard yang mantap untuk  kantoran', 'keyboard logitech'),
        array(2, 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'keyboard msi'),
        array(3, 'Mouse Genius', 50000, 'Mouse Genius biar lebih pintar', 'mouse genius'),
        array(4, 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'mouse jerry')
        );

        // Looping array multidimensi dan ubah menjadi array asosiatif
        $itemsAssoc = array();
        foreach ($items as $item) {
        $itemAssoc = array(
            'id' => $item[0],
            'name' => $item[1],
            'price' => $item[2],
            'description' => $item[3],
            'source(jpg)' => $item[4].'.jpg'
         );

         array_push($itemsAssoc, $itemAssoc);

        }
        // Cetak array asosiatif
        print_r($itemsAssoc);
        ?>

        <?php
        echo "<h3>Soal No 4 Asterix 5x5</h3>";
        for ($i = 1; $i <= 5; $i++) {
            for ($j = 1; $j <= (5 - $i); $j++) {
                echo "  ";
            }
            for ($k = 1; $k <= (2 * $i - 1); $k++) {
                echo "* ";
            }
            echo "\n";
        }
        ?>
    </body>
</html> 