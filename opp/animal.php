<!DOCTYPE html>
<html lang="en">
    <head>
        <h2>Release 0</h2>

    </head>
    <body>
        <h4>Supri Andriano</h4>
        <p>
            
            <?php
            class Animal {
            public $name;
            public $legs = 4;
            public $cold_blooded = "no";

            public function __construct($name) {
                $this->name = $name;
            }

            public function displayAnimal() {
                echo "Name: " . $this->name . "<br>";
                echo "Legs: " . $this->legs . "<br>";
                echo "Cold-blooded: " . $this->cold_blooded . "<br>";
            }
            }
            ?>
        </p>
    </body>
    </html>
