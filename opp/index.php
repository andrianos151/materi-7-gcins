<!DOCTYPE html>
<html lang="en">
    <head>
        <h2>Release 1</h2>

    </head>
    <body>
        <h4>Supri Andriano</h4>
        <p>
            <?php
            require_once('animal.php');

            // membuat objek dari class Animal
            $animal = new Animal("Kucing");

            // memanggil method displayAnimal() untuk menampilkan informasi hewan
            $animal->displayAnimal();

            // menampilkan output tambahan
            echo "Hewan " . $animal->name . " memiliki " . $animal->legs . " kaki dan cold-blooded = " . $animal->cold_blooded;
            ?>


            <?php
            require_once('Animal.php');
            require_once('Frog.php');
            require_once('Ape.php');

            // membuat objek dari class Animal, Frog, dan Ape
            $animal = new Animal("Kucing");
            $frog = new Frog("Katak");
            $ape = new Ape("Kera");

            // memanggil method displayAnimal() pada objek Animal
            $animal->displayAnimal();

            // memanggil method jump() pada objek Frog
            $frog->jump();

            // memanggil method displayAnimal() pada objek Frog
            $frog->displayAnimal();

            // memanggil method yell() pada objek Ape
            $ape->yell();

            // memanggil method displayAnimal() pada objek Ape
            $ape->displayAnimal();
            ?>
        </p>
    </body>
</html>